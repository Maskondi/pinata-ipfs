const fs = require('fs');
require("dotenv").config();
const yourPinataApiKey = "8af5fe117402815d8d55";
const yourPinataSecretApiKey = "c1ea30b2f3b75a45e2c3fce68e09f401ba94d001bc0b2392ba84c60fa4335057";

const pinataSDK = require('@pinata/sdk'); //npm install --save @pinata/sdk
const pinata = pinataSDK(yourPinataApiKey, yourPinataSecretApiKey);

pinata.testAuthentication().then((result) => {
    //handle successful authentication here
    console.log(result);
}).catch((err) => {
    //handle error here
    console.log(err);
});

const readableStreamForFile = fs.createReadStream('./boredape.webp');
const options = {
    pinataMetadata: {
        name: "BoredApe",
        keyvalues: {
            customKey: 'customValue',
            customKey2: 'customValue2'
        }
    },
    pinataOptions: {
        cidVersion: 0
    }
};
pinata.pinFileToIPFS(readableStreamForFile).then((result) => {
    //handle results here
    console.log(result);
}).catch((err) => {
    //handle error here
    console.log(err);
});
